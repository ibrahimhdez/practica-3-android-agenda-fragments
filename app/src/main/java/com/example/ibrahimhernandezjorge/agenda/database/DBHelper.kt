package com.example.ibrahimhernandezjorge.agenda.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.ibrahimhernandezjorge.agenda.Contacto

class DBHelper internal constructor(context : Context) : SQLiteOpenHelper(context, "database", null, 1){
    companion object {
        //Constante que contiene la sentencia SQL de creación de la tabla CONTACTOS
        const val SQL_CREATE_CONTACTOS =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_CONTACTOS + "(" +
                        DBContract.DBEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DBContract.DBEntry.NOMBRE + " TEXT(50), " +
                        DBContract.DBEntry.DIRECCION + " TEXT(200), " +
                        DBContract.DBEntry.MOVIL + " INTEGER(20), " +
                        DBContract.DBEntry.TELEFONO + " INTEGER(20), " +
                        DBContract.DBEntry.CORREO + " TEXT(60)" +
                        ");"

        const val SQL_CREATE_LLAMADAS_RECIENTES =
                "CREATE TABLE " + DBContract.DBEntry.TABLA_LLAMADAS_RECIENTES + "(" +
                        DBContract.DBEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        DBContract.DBEntry.ID_CONTACTO + " TEXT(50), " +
                        DBContract.DBEntry.TELEFONO + " INTEGER(20), " +
                        "FOREIGN KEY( " + DBContract.DBEntry.ID_CONTACTO + ") REFERENCES " +
                        DBContract.DBEntry.TABLA_CONTACTOS + "(${DBContract.DBEntry.ID}));"
    }

    /**
     * Método que se inicia en primer momento. Crea todas las tablas
     * @param db Objeto SQLiteDatabase
     */
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_CONTACTOS)
        db?.execSQL(SQL_CREATE_LLAMADAS_RECIENTES)
    }

    /**
     * Método que se ejecuta cuando se actualiza la base de datos
     * @param db Objeto SQLiteDatabase
     * @param p1 versión anterior
     * @param p2 nueva versión
     */
    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        destroy(db)
        onCreate(db)
    }

    /**
     * Método que destruye las tablas de la base de datos
     * @param db Writable de la base de datos
     */
    private fun destroy(db: SQLiteDatabase?) {
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_CONTACTOS}")
        db?.execSQL("DROP TABLE IF EXISTS ${DBContract.DBEntry.TABLA_LLAMADAS_RECIENTES}")
    }

    /**
     * Nétodo para insertar un contacto en la base de datos
     * @param contacto Objeto contacto que contendrá la información del contacto a insertar
     */
    fun insertContact(contacto: Contacto) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction() //Comienza la transacción

        //Inserta los datos de un usuario en un registro
        registro.put(DBContract.DBEntry.NOMBRE, contacto.nombre)
        registro.put(DBContract.DBEntry.DIRECCION, contacto.direccion)
        registro.put(DBContract.DBEntry.MOVIL, contacto.movil)
        registro.put(DBContract.DBEntry.TELEFONO, contacto.telefono)
        registro.put(DBContract.DBEntry.CORREO, contacto.correo)

        //Inserta el registro en la base de datos
        bd.insert(DBContract.DBEntry.TABLA_CONTACTOS, null, registro)
        closeTransaction(bd)
    }

    fun insertLlamada(contacto: Contacto, telefono: Int) {
        val bd = this.writableDatabase
        val registro = ContentValues()

        bd.beginTransaction()

        registro.put(DBContract.DBEntry.ID_CONTACTO, contacto.id)
        registro.put(DBContract.DBEntry.TELEFONO, telefono)

        bd.insert(DBContract.DBEntry.TABLA_LLAMADAS_RECIENTES, null, registro)
        closeTransaction(bd)
    }

    /**
     * Método que devuelve el último contacto insertado en la base de datos
     * @return Objeto Contacto del último insertado
     */
    fun getLastContact(): Contacto {
        val cursor = writableDatabase.rawQuery("SELECT MAX(${DBContract.DBEntry.ID}) FROM ${DBContract.DBEntry.TABLA_CONTACTOS};", null)
        cursor.moveToFirst()

        val contacto = getContacto(cursor.getInt(0))!!
        cursor.close()

        return contacto
    }

    /**
     * Método para borrar un contacto
     * @param id ID del contacto a borrar
     */
    fun deleteContact(id: Int) {
        val bd = this.writableDatabase

        bd.execSQL("PRAGMA foreign_keys=ON;")
        bd.beginTransaction()
        bd.delete(DBContract.DBEntry.TABLA_CONTACTOS, "${DBContract.DBEntry.ID}=$id", null)
        closeTransaction(bd)
    }

    /**
     * Método para cambiar el valor de un atributo de una determinada tabla de la base de datos
     * @param tableName Nombre de la tabla donde se efectuará el cambio
     * @param attribute Nombre del atributo que cambiará de valor
     * @param value Nuevo valor que tomará el atributo
     * @param primaryKey Nombre de la clave primaria de la tabla
     * @param valuePrimaryKey Valor de la clave primaria de la tabla
     */
    fun updateValue(tableName: String, attribute: String, value: String, primaryKey: String, valuePrimaryKey: String) {
        val bd = this.writableDatabase

        bd.beginTransaction()

        bd.execSQL("UPDATE $tableName SET $attribute='$value' WHERE $primaryKey='$valuePrimaryKey' ")
        closeTransaction(bd)
    }

    /**
     * Método que dado el ID de un Contacto realiza una consulta a la base de datos y devuelve un objeto Contacto
     * @param id ID del contacto que queremos obtener
     * @return Objeto Contacto que corresponde al ID pasado por parámetros
     */
    fun getContacto(id: Int): Contacto? {
        val cursor = cursorConsulta(DBContract.DBEntry.TABLA_CONTACTOS, arrayOf(DBContract.DBEntry.ID),
                arrayOf(id.toString()))

        return when(cursor.moveToFirst()) {
            true ->
                Contacto(cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.ID)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NOMBRE)),
                        cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.DIRECCION)), cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.MOVIL)),
                        cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.CORREO)))

            false -> null
        }
    }

    /**
     * Método que cierra y confirma una transacción
     * @param bd Objeto base de datos con permisos de escritura en la que será confirmada la transacción
     */
    private fun closeTransaction(bd: SQLiteDatabase) {
        bd.setTransactionSuccessful() //Confirmar la transacción para que los cambios sean guardados
        bd.endTransaction() //Finalizar la transacción
        bd.close() //Cerrar base de datos
    }

    /**
     * Método para realizar una consulta personalizada
     * @return Cursor con el resultado de la consulta
     */
    private fun cursorConsultaPersonalizada(tabla: String, tableColumns: Array<String>?, whereClause: String?, whereArgs: Array<String>?,
                                            groupBy: String?, having: String?, orderBy: String?, limite: Int? = null): Cursor {
        return this.readableDatabase.query(tabla, tableColumns, whereClause, whereArgs, groupBy, having, orderBy, limite?.toString())
    }

    /**
     * Método para realizar consulta
     * @return Cursor con el resultado de la consulta
     */
    fun cursorConsulta(tabla: String, atributos: Array<String>?, valores: Array<String>?, limite: Int? = null): Cursor {
        return if (atributos != null) {
            var whereClause = atributos[0] + "= ?"
            for (i in 1 until atributos.size)
                whereClause += " AND " + atributos[i] + "= ?"
            cursorConsultaPersonalizada(tabla, null, whereClause, valores, null, null, null, limite)
        } else
            cursorConsultaPersonalizada(tabla, null, null, null, null, null, null, limite)
    }
}
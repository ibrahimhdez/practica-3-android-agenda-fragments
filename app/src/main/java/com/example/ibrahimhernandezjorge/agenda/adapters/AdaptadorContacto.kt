package com.example.ibrahimhernandezjorge.agenda.adapters

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.ibrahimhernandezjorge.agenda.Contacto
import com.example.ibrahimhernandezjorge.agenda.R
import java.util.ArrayList
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator

class AdaptadorContacto(contactos: ArrayList<Contacto>, infladorMenu: (ImageView, Int, Int) -> Unit, mostrarContacto: (CardView, Int, Int) -> Unit): RecyclerView.Adapter<AdaptadorContacto.ViewHolderPerson>() {
    private var contactos: ArrayList<Contacto>? = contactos
    private var metodoInfladorMenu = infladorMenu
    private var metodoMostrarContacto = mostrarContacto
    private val copyContactos = ArrayList<Contacto>()

    companion object {
        const val NOTHING = ""
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPerson {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_contact, parent, false)
        copyContactos.clear()
        copyContactos.addAll(contactos!!)

        return ViewHolderPerson(view)
    }

    override fun onBindViewHolder(holder: ViewHolderPerson, position: Int) {
        holder.nombre.text = contactos!![position].nombre

        //Comprobamos si existe un móvil para el contacto, si es 0, mostraremos en la tarjeta su teléfono fijo (siempre y cuando
        //este teléfono tampoco sea 0)
        holder.numero.text = if (contactos!![position].movil == 0 || contactos!![position].movil == null) {
            if (contactos!![position].telefono != 0 && contactos!![position].telefono != null)
                contactos!![position].telefono.toString()
            else
                NOTHING //Si los dos números son 0 o null, pondremos una cadena vacía
        }
        else
            contactos!![position].movil.toString()

        setContactImage(contactos!![position], holder.contactImage, contactos!![position].nombre[0])

        //Invocar el método que infla el menú de puntos
        metodoInfladorMenu(holder.menu, contactos!![position].id!!, position)

        //Invocar el método que infla el menú para llamar, enviar mensaje o ver el contacto pulsado
        metodoMostrarContacto(holder.cardView, contactos!![position].id!!, position)
    }

    override fun getItemCount(): Int {
        return contactos!!.size
    }

    private fun setContactImage(contacto: Contacto, contactImage: ImageView, letter: Char) {
        if (contacto.color == null) {
            val generator = ColorGenerator.MATERIAL
            contacto.color = generator.randomColor
        }

        val drawable = TextDrawable.builder()
                .buildRound(letter.toString(), contacto.color!!)

        contactImage.setImageDrawable(drawable)
    }

    fun filtrar(texto: String) {
        contactos!!.clear() //Limpiamos el array del adapter para introducir en el los contactos que se correspondan con la búsqueda

        //Si el texto a buscar esta vacío el resultado será todos los contactos
        if (texto.isEmpty())
            contactos!!.addAll(copyContactos)
        //Si no está vacío
        else {
            //Primero comprobamos si el texto es un número
            if (texto.toIntOrNull() != null) {
                //Buscamos si algún teléfono o móvil de un contacto coincide con la búsqueda
                for (contacto in copyContactos)
                    //Si coincide lo insertamos en el array
                    if(contacto.movil.toString().toLowerCase().contains(texto) || contacto.telefono.toString().toLowerCase().contains(texto))
                        contactos!!.add(contacto)
            } else
                //Lo mismo con los números pero esta vez con el nombre del contacto
                for (contacto in copyContactos)
                    if (contacto.nombre.toLowerCase().contains(texto))
                        contactos!!.add(contacto)
        }
        notifyDataSetChanged() //Notificamos al adapter que los datos han cambiado
    }

    inner class ViewHolderPerson(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombre: TextView = itemView.findViewById(R.id.nombre_contacto)
        var numero: TextView = itemView.findViewById(R.id.telefono_contacto)
        var menu: ImageView = itemView.findViewById(R.id.menu_imageview)
        var contactImage: ImageView = itemView.findViewById(R.id.contact_image)
        var cardView: CardView = itemView.findViewById(R.id.card_contacto)
    }
}

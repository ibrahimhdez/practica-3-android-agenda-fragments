package com.example.ibrahimhernandezjorge.agenda.`interface`

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.example.ibrahimhernandezjorge.agenda.Contacto
import com.example.ibrahimhernandezjorge.agenda.R
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import com.example.ibrahimhernandezjorge.agenda.database.DBContract
import android.app.Activity
import android.content.Intent
import android.text.Editable

class AddContactActivity : AppCompatActivity() {
    private var contactoActual: Contacto? = null
    private var editTextNombre: EditText? = null
    private var editTextDireccion: EditText? = null
    private var editTextMovil: EditText? = null
    private var editTextTelefono: EditText? = null
    private var editTextCorreo: EditText? = null
    private var optionsMenu: Menu? = null
    private var mostrando = false
    private var modificando = false

    companion object {
        const val NO_DATA = "NO_DATA"
        const val NOTHING = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_contact_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true) //Mostrar en el toolbar la flecha de hacia atrás
        iniciarComponentes()

        intent.extras?.getInt("mostrar_contacto")?.let {
            if(it != 0) {
                contactoActual = DBComunicator.getInstance().getContacto(it)
                mostrando = true
                rellenarEditTexts(false)
            }
        }

        intent.extras?.getInt("modificar_contacto")?.let {
            if(it != 0) {
                contactoActual = DBComunicator.getInstance().getContacto(it)
                modificando = true
                rellenarEditTexts(true)
            }
        }
    }

    /**
     * Método para iniciar los componentes del layout del Activity
     */
    private fun iniciarComponentes() {
        editTextNombre = findViewById(R.id.EditTextNombre)
        editTextDireccion = findViewById(R.id.EditTextDireccion)
        editTextMovil = findViewById(R.id.EditTextMovil)
        editTextTelefono = findViewById(R.id.EditTextTelefono)
        editTextCorreo = findViewById(R.id.EditTextCorreo)
    }

    /**
     * Método que rellena los EditTexts con los valores de los campos si estamos visualizando o editando un contacto
     * @param editable Si estamos visualizando el contacto será false, si estamos editando será true
     */
    private fun rellenarEditTexts(editable: Boolean) {
        editTextNombre!!.setText(contactoActual!!.nombre)
        editTextNombre!!.isEnabled = editable

        editTextDireccion!!.text = if (contactoActual!!.direccion.equals(NO_DATA))
            Editable.Factory.getInstance().newEditable(NOTHING)
        else
            Editable.Factory.getInstance().newEditable(contactoActual!!.direccion)

        editTextDireccion!!.isEnabled = editable

        editTextMovil!!.text = if (contactoActual!!.movil == 0)
            Editable.Factory.getInstance().newEditable(NOTHING)
        else
            Editable.Factory.getInstance().newEditable(contactoActual!!.movil.toString())

        editTextMovil!!.isEnabled = editable

        editTextTelefono!!.text = if (contactoActual!!.telefono == 0)
            Editable.Factory.getInstance().newEditable(NOTHING)
        else
            Editable.Factory.getInstance().newEditable(contactoActual!!.telefono.toString())

        editTextTelefono!!.isEnabled = editable

        editTextCorreo!!.text = if(contactoActual!!.correo.equals(NO_DATA))
            Editable.Factory.getInstance().newEditable(NOTHING)
        else
            Editable.Factory.getInstance().newEditable(contactoActual!!.direccion)

        editTextCorreo!!.isEnabled = editable
    }

    /**
     * Método que comprueba si el nombre del contacto está relleno
     * @return True si está vacío, false si está completo
     */
    private fun nombreVacio(): Boolean {
        return editTextNombre!!.text.isEmpty()
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            //Si pulsan el botón de añadir del menú de la toolbar del Activity
            R.id.action_add -> {
                if(!nombreVacio()) {
                    val contactoActual = generarContactoActual()
                    DBComunicator.getInstance().insertContact(contactoActual) //Insertar en la base de datos el contacto actual
                    Toast.makeText(this, "Contacto creado", Toast.LENGTH_LONG).show()

                    val intent = Intent()
                    intent.putExtra(DBContract.DBEntry.ID, DBComunicator.getInstance().getLastContact().id)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
                else
                    Toast.makeText(this, "Debe de insertar un nombre al contacto", Toast.LENGTH_LONG).show()
            }

            //Si editamos el contacto lo modificamos en la base de datos
            R.id.action_save -> {
                DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_CONTACTOS,
                        DBContract.DBEntry.NOMBRE, editTextNombre!!.text.toString(), DBContract.DBEntry.ID, contactoActual!!.id.toString())

                //En el caso de que se introduzcan únicamente espacions vacíos en el EditText de la dirección, los descartamos
                val textDireccion = if (editTextMovil!!.text.toString().replace(" ", "") == "")
                    NO_DATA
                else
                    editTextMovil!!.text.toString()


                DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_CONTACTOS,
                        DBContract.DBEntry.DIRECCION, textDireccion, DBContract.DBEntry.ID, contactoActual!!.id.toString())

                DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_CONTACTOS,
                        DBContract.DBEntry.MOVIL, editTextMovil!!.text.toString(), DBContract.DBEntry.ID, contactoActual!!.id.toString())

                DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_CONTACTOS,
                        DBContract.DBEntry.TELEFONO, editTextTelefono!!.text.toString(), DBContract.DBEntry.ID, contactoActual!!.id.toString())

                //En el caso de que se introduzcan únicamente espacions vacíos en el EditText del correo, los descartamos
                val textCorreo = if (editTextCorreo!!.text.toString().replace(" ", "") == "")
                    NO_DATA
                else
                    editTextCorreo!!.text.toString()

                DBComunicator.getInstance().updateValue(DBContract.DBEntry.TABLA_CONTACTOS,
                        DBContract.DBEntry.CORREO, textCorreo, DBContract.DBEntry.ID, contactoActual!!.id.toString())

                Toast.makeText(this, "Contacto modificado", Toast.LENGTH_LONG).show()

                setResult(Activity.RESULT_OK)
                finish() //Terminamos la Activity una vez modificado el contacto
            }

            //Si estamos visualizando un contacto y seleccionamos la opción de editarlo, pasamos a modificar el Activity
            R.id.action_edit -> {
                mostrando = false
                modificando = true
                onCreateOptionsMenu(optionsMenu!!)
                rellenarEditTexts(true)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método que genera un objeto Contacto con los datos actuales de los EditText
     * @return Objeto contacto
     */
    private fun generarContactoActual(): Contacto {
        val contacto = Contacto(editTextNombre!!.text.toString(),
                editTextDireccion!!.text.toString(),
                editTextMovil!!.text.toString().toIntOrNull(),
                editTextTelefono!!.text.toString().toIntOrNull(),
                editTextCorreo!!.text.toString())

        contacto.correo.isNullOrEmpty().let { contacto.correo = NO_DATA }
        contacto.direccion.isNullOrEmpty().let { contacto.direccion = NO_DATA }

        return contacto
    }

    /**
     * Método para añadir menú de añadir a la toolbar del Activity
     * @param menu Menu del Activity
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        optionsMenu = menu
        optionsMenu!!.clear()

        val tipoMenu = when {
            modificando -> R.menu.menu_save
            mostrando -> R.menu.menu_edit
            else -> R.menu.menu_add
        }
        menuInflater.inflate(tipoMenu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Método para volver a la anterior Activity con el botón del toolbar
     */
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }
}
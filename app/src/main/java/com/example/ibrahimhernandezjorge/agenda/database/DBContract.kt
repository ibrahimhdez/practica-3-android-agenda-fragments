package com.example.ibrahimhernandezjorge.agenda.database

object DBContract {
    class DBEntry {
        companion object {
            const val TABLA_CONTACTOS = "CONTACTOS"
            const val TABLA_LLAMADAS_RECIENTES = "LLAMADAS_RECIENTES"
            const val ID = "ID"
            const val ID_CONTACTO = "ID_CONTACTO"
            const val NOMBRE = "NOMBRE"
            const val DIRECCION = "DIR"
            const val MOVIL = "MOV"
            const val TELEFONO = "TLF"
            const val CORREO = "MAIL"
        }
    }
}
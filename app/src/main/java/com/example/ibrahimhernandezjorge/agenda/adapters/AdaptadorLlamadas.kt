package com.example.ibrahimhernandezjorge.agenda.adapters

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ibrahimhernandezjorge.agenda.R
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator

class AdaptadorLlamadas(llamadas: ArrayList<Int>, cardLlamadaListener: (CardView, Int) -> Unit): RecyclerView.Adapter<AdaptadorLlamadas.ViewHolderLlamadas>() {
    private var llamadas: ArrayList<Int> = llamadas
    private var cardListener = cardLlamadaListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderLlamadas {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_llamada, parent, false)

        return ViewHolderLlamadas(view)
    }

    override fun onBindViewHolder(holder: ViewHolderLlamadas, position: Int) {
        val idContacto = llamadas[position]
        val contacto = DBComunicator.getInstance().getContacto(idContacto)

        holder.nombre.text = contacto!!.nombre

        cardListener(holder.cardView, idContacto)
    }

    override fun getItemCount(): Int {
        return llamadas.size
    }


    inner class ViewHolderLlamadas(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombre: TextView = itemView.findViewById(R.id.nombre_llamada)
        var cardView: CardView = itemView.findViewById(R.id.card_llamada)
    }
}
package com.example.ibrahimhernandezjorge.agenda.fragments

import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.example.ibrahimhernandezjorge.agenda.Contacto
import com.example.ibrahimhernandezjorge.agenda.R
import com.example.ibrahimhernandezjorge.agenda.adapters.AdaptadorLlamadas
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import com.example.ibrahimhernandezjorge.agenda.database.DBContract

class LlamadasFragment: Fragment() {
    private var vista: View? = null
    private var llamadas: ArrayList<Int>? = null
    var listener: View.OnClickListener? = null
    var recyclerLlamadas: RecyclerView? = null
    var adapter: AdaptadorLlamadas? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vista = inflater.inflate(R.layout.llamadas_fragment, container, false)

        return vista
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(llamadas == null)
            llamadas = getLlamadas()

        recyclerLlamadas = vista!!.findViewById(R.id.recyclerViewLlamadas)
        recyclerLlamadas!!.layoutManager = LinearLayoutManager(vista!!.context)

        iniciarRecycler()
    }

    private fun iniciarRecycler() {
        adapter = AdaptadorLlamadas(llamadas!!, ::cardLlamadaListener)
        recyclerLlamadas!!.adapter = adapter
    }

    private fun getLlamadas(): ArrayList<Int> {
        val llamadas = ArrayList<Int>()
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_LLAMADAS_RECIENTES, null, null)

        while (cursor.moveToNext()) {
            val llamada = cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.ID_CONTACTO))

            llamadas.add(llamada)
        }
        return llamadas
    }

    fun addLlamada(contacto: Contacto) {
        llamadas!!.add(contacto.id!!)
        adapter!!.notifyItemInserted(llamadas!!.count())
    }

    fun cardLlamadaListener(cardView: CardView, id: Int) {
        cardView.setOnClickListener {
            val intent = Intent(Intent.ACTION_CALL)
            val numero: Int?
            val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_LLAMADAS_RECIENTES, arrayOf(DBContract.DBEntry.ID_CONTACTO),
                    arrayOf(id.toString()))

            numero = if(cursor.moveToFirst())
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO))
            else
                null

            intent.data = Uri.parse("tel:$numero")
            startActivity(intent)
        }
    }
}
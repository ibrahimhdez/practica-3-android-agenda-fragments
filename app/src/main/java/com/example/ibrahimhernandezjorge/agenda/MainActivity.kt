package com.example.ibrahimhernandezjorge.agenda

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import android.app.SearchManager
import android.content.Context
import android.support.v7.widget.SearchView
import android.content.pm.PackageManager
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.ibrahimhernandezjorge.agenda.fragments.ContactosFragment
import com.example.ibrahimhernandezjorge.agenda.fragments.LlamadasFragment
import kotlinx.android.synthetic.*

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener, View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private var exportImportController: ExportImportController? = null
    private var menu: Menu? = null
    private var searchView: SearchView? = null
    private var fragmentContactos: ContactosFragment? = null
    private var fragmentLlamadas: LlamadasFragment? = null
    private var bottomNavigationView: BottomNavigationView? = null
    private var fragmentTransaction: FragmentTransaction? = null

    companion object {
        const val PERMISSIONS_REQUEST_CALL_PHONE = 0
        const val PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 1
        const val ADD_CONTACT_CODE = 1
        const val EDIT_CONTACT_CODE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        initFragments()
        initFragmentsArguments()
        initEventosFragments()

        addFragmentTransaction(fragmentLlamadas!!)
        addFragmentTransaction(fragmentContactos!!)
        replaceFragmentTransaction(fragmentContactos!!)

        pedirPermisosWriteExternalStorage()
        pedirPermisosReadExternalStorage()
    }

    /**
     * Método para iniciar los componentes del Activity
     */
    private fun iniciarComponentes() {
        DBComunicator.initInstance(this) //Inicializar la base de datos
        exportImportController = ExportImportController()
        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView!!.menu.getItem(1).isChecked = true
    }

    /**
     * Método donde se asignan los eventos pertenecientes al fragment
     */
    private fun initEventosFragments() {
        fragmentContactos!!.listener = this
        fragmentLlamadas!!.listener = this
        bottomNavigationView!!.setOnNavigationItemSelectedListener(this)
    }

    private fun initFragments() {
        fragmentContactos = ContactosFragment()
        fragmentLlamadas = LlamadasFragment()

        fragmentContactos!!.addLlamada = fragmentLlamadas!!::addLlamada
    }

    private fun initFragmentsArguments() {
        val contactosArguments = Bundle(1)
        val llamadasArguments = Bundle(1)

        contactosArguments.putSerializable("contactos", fragmentContactos!!.contactos)
        llamadasArguments.putSerializable("llamadas", fragmentContactos!!.contactos) //TODO CAMBIAR A LLAMADAS

        fragmentContactos!!.arguments = contactosArguments
        fragmentLlamadas!!.arguments = llamadasArguments
    }

    private fun addFragmentTransaction(fragment: Fragment) {
        fragmentTransaction = supportFragmentManager.beginTransaction()

        fragmentTransaction!!.add(R.id.frame_layout, fragment)

        fragmentTransaction!!.commit()
    }

    private fun replaceFragmentTransaction(fragment: Fragment) {
        fragmentTransaction = supportFragmentManager.beginTransaction()

        fragmentTransaction!!.replace(R.id.frame_layout, fragment)
        fragmentTransaction!!.addToBackStack(null)

        fragmentTransaction!!.commit()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when(menuItem.itemId) {
            R.id.item_navigation_contactos -> {
                menuItem.isChecked = true
                replaceFragmentTransaction(fragmentContactos!!)
            }

            R.id.item_navigation_llamadas -> {
                menuItem.isChecked = true
                replaceFragmentTransaction(fragmentLlamadas!!)
            }
        }
        return true
    }

    override fun onClick(v: View?) {

    }

    override fun onQueryTextChange(newText: String?): Boolean {
        fragmentContactos!!.adapter!!.filtrar(newText!!.toLowerCase())
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        val view = this.currentFocus
        view!!.clearFindViewByIdCache()

        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

        return true
    }


    /**
     * Método para añadir menú de añadir a la toolbar del Activity
     * @param menu Menu del Activity
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu.findItem(R.id.action_search)
        searchView = searchMenuItem.actionView as SearchView
        searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView!!.isSubmitButtonEnabled = true
        searchView!!.onFocusChangeListener
        searchView!!.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }

    private fun pedirPermisosWriteExternalStorage() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private fun pedirPermisosReadExternalStorage() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}

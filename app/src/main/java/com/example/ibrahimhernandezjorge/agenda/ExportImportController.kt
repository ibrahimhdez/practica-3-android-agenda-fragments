package com.example.ibrahimhernandezjorge.agenda

import android.content.Context
import android.os.Environment
import com.google.gson.Gson
import android.util.Log
import android.widget.Toast
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import com.google.gson.internal.LinkedTreeMap
import java.io.*

class ExportImportController {
    private var gson = Gson()

    fun exportContacts(contactos: ArrayList<Contacto>) {
        writeExternalStorage(obtainJson(contactos))
    }

    fun importContacts(context: Context, contactosActuales: ArrayList<Contacto>, addContacto: (Int) -> Unit) {
        val directorio = File("${Environment.getExternalStorageDirectory()}/directorioAgenda")
        val file = File(directorio, "misContactos.CNT")

        if(!directorio.exists() || !file.exists())
            Toast.makeText(context, "No existe copia de contactos en el almacenamiento externo.", Toast.LENGTH_LONG).show()
        else {
            val contactosJson = gson.fromJson(file.bufferedReader().readLine(), ArrayList<LinkedTreeMap<String,String>>().javaClass)
            var nuevosContactos = false

            for(contactoJson in contactosJson) {
                val contacto = gson.fromJson(contactoJson.toString(), Contacto::class.java)

                if(!contactosActuales.contains(contacto)) {
                    nuevosContactos = true
                    DBComunicator.getInstance().insertContact(contacto)
                    addContacto.invoke(DBComunicator.getInstance().getLastContact().id!!)
                }
            }

            if(!nuevosContactos)
                Toast.makeText(context, "No hay cambios aplicables", Toast.LENGTH_LONG).show()
        }
    }

    private fun obtainJson(contactos: ArrayList<Contacto>): String {
        return gson.toJson(contactos)
    }

    private fun writeExternalStorage(jsonString: String) {
        val directorio = File("${Environment.getExternalStorageDirectory()}/directorioAgenda")
        if(!directorio.exists())
            directorio.mkdirs()

        val file = File(directorio, "misContactos.CNT")
        try {
            val fileOutputStream = FileOutputStream(file)
            val printWriter = PrintWriter(fileOutputStream)

            printWriter.println(jsonString)
            printWriter.flush()
            printWriter.close()
            fileOutputStream.close()
        } catch (e: FileNotFoundException) {
            Log.e("File Not Found", "Error " + e.message)
        } catch (e: IOException) {
            Log.e("IO Exception", "Error " + e.message)
        }
    }
}
package com.example.ibrahimhernandezjorge.agenda

import android.os.Parcel
import android.os.Parcelable

class Contacto (var id: Int? = null, var nombre: String = "", var direccion: String? = null,
                           var movil: Int? = null, var telefono: Int? = null,
                           var correo: String? = null, var color: Int? = null): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()!!)

    constructor(nombre: String = "", direccion: String? = "*",
                movil: Int? = null, telefono: Int? = null,
                correo: String? = "*") : this(null, nombre, direccion, movil, telefono, correo, null)


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(nombre)
        parcel.writeString(direccion)
        parcel.writeValue(movil)
        parcel.writeValue(telefono)
        parcel.writeString(correo)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        return (other as Contacto).nombre == this.nombre &&
                other.movil == this.movil &&
                other.direccion == this.direccion &&
                other.telefono == this.telefono &&
                other.correo == this.correo
    }

    companion object CREATOR : Parcelable.Creator<Contacto> {
        override fun createFromParcel(parcel: Parcel): Contacto {
            return Contacto(parcel)
        }

        override fun newArray(size: Int): Array<Contacto?> {
            return arrayOfNulls(size)
        }
    }
}
package com.example.ibrahimhernandezjorge.agenda.fragments

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast
import com.example.ibrahimhernandezjorge.agenda.Contacto
import com.example.ibrahimhernandezjorge.agenda.ExportImportController
import com.example.ibrahimhernandezjorge.agenda.MainActivity
import com.example.ibrahimhernandezjorge.agenda.R
import com.example.ibrahimhernandezjorge.agenda.`interface`.AddContactActivity
import com.example.ibrahimhernandezjorge.agenda.adapters.AdaptadorContacto
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import com.example.ibrahimhernandezjorge.agenda.database.DBContract

class ContactosFragment: Fragment(), View.OnClickListener {
    private var vista: View? = null
    var contactos: ArrayList<Contacto>? = null
    private var recyclerContactos: RecyclerView? = null
    private var fab: FloatingActionButton? = null
    private var exportImportController: ExportImportController? = null
    var adapter: AdaptadorContacto? = null
    private var posicionContactoEditado: Int? = null
    private var idContactoEditado: Int? = null
    var listener: View.OnClickListener? = null
    var addLlamada: ((Contacto) -> Unit)? = null
    private var numberToCall = 0
    private var idContactoLlamada: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        vista = inflater.inflate(R.layout.contactos_fragment, container, false)

        return vista
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(contactos == null)
            contactos = obtenerContactos()

        recyclerContactos = vista!!.findViewById(R.id.RecyclerContactos)
        recyclerContactos!!.layoutManager = LinearLayoutManager(vista!!.context)
        iniciarRecycler()

        fab = vista!!.findViewById(R.id.fab)
        fab!!.setOnClickListener { _ ->
            startActivityForResult(Intent(vista!!.context, AddContactActivity::class.java), MainActivity.ADD_CONTACT_CODE)
        }
    }

    /**
     * Método para actualizar el RecyclerView del Activity
     */
    private fun iniciarRecycler() {
        sortArrayContactos()
        //Obtener el adapter del contacto para añadírselo al RecyclerView de los grupos
        adapter = AdaptadorContacto(contactos!!, ::inflateMenuContactos, ::mostrarContacto)
        recyclerContactos!!.adapter = adapter
    }


    /**
     * Método que obtiene de la Base de Datos los contactos
     * @return ArrayList con todos los contactos alojados en la Base de Datos local
     */
    private fun obtenerContactos(): ArrayList<Contacto>? {
        val arrayContactos = ArrayList<Contacto>()
        //Cursor que contiene los resultados de la llamada a la tabla "CONTACTOS"
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_CONTACTOS, null, null)

        //Bucle que va sacando del cursor todas los contactos
        while (cursor.moveToNext()) {
            //Comprobamos si el valor ID es null, si no lo es guardamos el número que corresponde
            //Se realiza esta comprobación para evitar obtener un 0 si el valor es null
            val id = if(cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.ID)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.ID))

            //Comprobamos si el valor MOVIL es null, si no lo es guardamos el número que corresponde
            val movil = if (cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.MOVIL)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.MOVIL))

            //Comprobamos si el valor TELEFONO es null
            val telefono = if (cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO))

            //Obtenemos el objeto Contacto con los valores sacados de la base de datos
            val contacto = Contacto(id, cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NOMBRE)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.DIRECCION)),
                    movil, telefono, cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.CORREO)))

            //Guardamos el objeto Contacto en el array de contactos
            arrayContactos.add(contacto)
        }
        cursor.close()

        return arrayContactos
    }

    private fun inflateMenuContactos(menu: ImageView, id: Int, pos: Int) {
        menu.setOnClickListener{
            val popupMenu = PopupMenu(menu.context, menu)
            popupMenu.menuInflater.inflate(R.menu.menu_contact, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener{ item: MenuItem? ->
                when (item!!.itemId) {
                    R.id.editar_contacto -> {
                        posicionContactoEditado = pos //Guardamos la posición para modificar el contacto en el ArrayList una vez el usuario lo modifique
                        idContactoEditado = id //Guardamos el ID para sacar de la Base de Datos el contacto modificado cuando el usuario termine

                        val intent = Intent(vista!!.context, AddContactActivity::class.java)
                        intent.putExtra("modificar_contacto", id)
                        startActivityForResult(intent, MainActivity.EDIT_CONTACT_CODE)
                    }

                    R.id.eliminar_contacto -> {
                        confirmarEliminacion(id, pos)
                    }
                }
                true
            }
            popupMenu.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK) {
            //Si volvemos de crear un contacto, lo añadimos
            if (requestCode == MainActivity.ADD_CONTACT_CODE)
                addContacto(data!!.getIntExtra(DBContract.DBEntry.ID, 9999))

            //Si volvemos de editar un contacto, actualizamos el recycler
            else if (requestCode == MainActivity.EDIT_CONTACT_CODE) {
                contactos!![posicionContactoEditado!!] = DBComunicator.getInstance().getContacto(idContactoEditado!!)!!
                adapter!!.notifyItemChanged(posicionContactoEditado!!)
                adapter!!.notifyDataSetChanged()
            }
        }
    }

    private fun mostrarContacto(cardView: CardView, id: Int, pos: Int) {
        cardView.setOnClickListener{
            val popupMenu = PopupMenu(cardView.context, cardView)
            popupMenu.menuInflater.inflate(R.menu.menu_tlf, popupMenu.menu)
            //Forzar a mostrar los iconos en el menú PopUp
            try {
                val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible = true
                val mPopup = fieldMPopup.get(popupMenu)
                mPopup.javaClass
                        .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                        .invoke(mPopup, true)
            } catch (e: Exception){

            } finally {
                popupMenu.show()
            }

            initContactPopUpMenuEvents(popupMenu, id, pos)
        }
    }

    private fun initContactPopUpMenuEvents(popupMenu: PopupMenu, id: Int, pos: Int) {
        popupMenu.setOnMenuItemClickListener{ item: MenuItem? ->
            when (item!!.itemId) {
                R.id.llamar_contacto -> {
                    var llamar = true
                    val numero = when {
                        DBComunicator.getInstance().getContacto(id)!!.movil != 0 -> DBComunicator.getInstance().getContacto(id)!!.movil
                        DBComunicator.getInstance().getContacto(id)!!.telefono != 0 -> DBComunicator.getInstance().getContacto(id)!!.telefono
                        else -> null
                    }
                    idContactoLlamada = id

                    if(numero != null)

                        numberToCall = numero
                    else {
                        Toast.makeText(vista!!.context, "El contacto no tiene ningún número", Toast.LENGTH_LONG).show()
                        llamar = false
                    }

                    if(llamar)
                        pedirPermisosLlamadas()

                }

                R.id.mensaje_contacto -> {
                    val uri = Uri.parse("smsto:${DBComunicator.getInstance().getContacto(id)!!.movil}")
                    val intent = Intent(Intent.ACTION_SENDTO, uri)
                    intent.putExtra("sms_body", "")
                    startActivity(intent)
                }

                R.id.info_contacto -> {
                    val intent = Intent(vista!!.context, AddContactActivity::class.java)
                    posicionContactoEditado = pos
                    idContactoEditado = id

                    intent.putExtra("mostrar_contacto", id)
                    startActivityForResult(intent, MainActivity.EDIT_CONTACT_CODE)
                }
            }
            true
        }
    }

    private fun registrarLlamada(contacto: Contacto, telefono: Int) {
        DBComunicator.getInstance().insertLlamada(contacto, telefono)

        addLlamada!!.invoke(contacto)
    }

    private fun confirmarEliminacion(id: Int, pos: Int) {
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    eliminarContacto(id, pos)
                    Toast.makeText(vista!!.context, "Contacto eliminado", Toast.LENGTH_LONG).show()
                }

                DialogInterface.BUTTON_NEGATIVE -> {

                }
            }
        }

        val builder = AlertDialog.Builder(vista!!.context)
        builder.setMessage("¿Desea eliminar el contacto?").setPositiveButton("Sí", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show()
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //vista!!.onBackPressed()
                return true
            }

            //Si pulsan el botón de añadir del menú de la toolbar del Activity
            R.id.action_export -> {
                exportImportController!!.exportContacts(contactos!!)
                Toast.makeText(vista!!.context, "Contactos exportados", Toast.LENGTH_LONG).show()
            }

            R.id.action_import ->
                exportImportController!!.importContacts(vista!!.context, contactos!!, ::addContacto)
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método para añadir un contacto al RecyclerView
     * @param id ID del contacto a insertar
     */
    private fun addContacto(id: Int) {
        contactos!!.add(DBComunicator.getInstance().getContacto(id)!!)
        sortArrayContactos()
        adapter = AdaptadorContacto(contactos!!, ::inflateMenuContactos, ::mostrarContacto)
        recyclerContactos!!.adapter = adapter
    }

    /**
     * Método para eliminar un contacto del RecyclerView
     * @param id ID del contacto a eliminar
     * @param pos Posición que ocupa el contacto a eliminar
     */
    private fun eliminarContacto(id: Int, pos: Int) {
        //Borrar el contacto de la base de datos
        DBComunicator.getInstance().deleteContact(id)
        //Eliminar el contacto del array
        contactos!!.removeAt(pos)
        //Notificar al adaptador que un item ha sido borrado
        adapter!!.notifyItemRemoved(pos)
        adapter!!.notifyDataSetChanged()
    }

    private fun sortArrayContactos() {
        contactos = ArrayList(contactos!!.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.nombre }))
    }

    override fun onClick(v: View?) {
        listener!!.onClick(v)
    }

    private fun callNumber() {
        val intent = Intent(Intent.ACTION_CALL)

        registrarLlamada(DBComunicator.getInstance().getContacto(idContactoLlamada!!)!!, numberToCall)
        intent.data = Uri.parse("tel:$numberToCall")
        startActivity(intent)
    }

    private fun pedirPermisosLlamadas() {
        if (ActivityCompat.checkSelfPermission(vista!!.context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(vista!!.context as Activity,
                            Manifest.permission.CALL_PHONE))
            else
                ActivityCompat.requestPermissions(vista!!.context as Activity,
                        arrayOf(Manifest.permission.CALL_PHONE), MainActivity.PERMISSIONS_REQUEST_CALL_PHONE)
        }
        else
            callNumber()
    }
}
